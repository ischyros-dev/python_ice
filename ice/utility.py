import json
from ice import ssm_client, terraform_instance_id, ec2_image


def parse_value(attribute, depth=0):
    if isinstance(attribute, str):
        output = f'"{attribute}"'
        return output

    if isinstance(attribute, list):
        output = '['
        for item in attribute:
            if item != attribute[-1]:
                output += f'{parse_value(item, depth + 1)},'
            else:
                output += f'{parse_value(item, depth + 1)}'
        output += ']'
        return output

    if isinstance(attribute, dict):
        output = '{'
        for key, value in attribute.items():
            output += ('\n' + '  ' * (depth + 1) + f'{key} = ' + f'{parse_value(value, depth + 1)}')
        output += ('\n' + '  ' * depth + '}')
        return output

    return attribute


class ConfigObj:
    def __init__(self, classifier: str, conf_type: str, name: str):
        self.classifier = classifier
        self.conf_type = conf_type
        self.name = name
        self.dependencies = {}
        self.attributes = {}
        self.configs = {}

    def add_dependency_variables(self, dependency: {}):
        self.dependencies.update(dependency)

    def add_attr(self, attributes: {}):
        self.attributes.update(attributes)

    def add_config_block(self, config: {}):
        self.configs.update(config)

    def dump(self):
        if self.name == '':
            output = f'{self.classifier} "{self.conf_type}" ' + '{'
        else:
            output = f'{self.classifier} "{self.conf_type}" "{self.name}" ' + '{'

        for dep_key, dep_value in self.dependencies.items():
            output += f'\n  {dep_key} = {dep_value}'

        for attr_key, attr_value in self.attributes.items():
            output += f'\n  {attr_key} = {parse_value(attr_value, 1)}'

        for conf_key, conf_value in self.configs.items():
            output += f'\n\n  {conf_key} {parse_value(conf_value, 1)}'
        output += '\n}'

        return output


def send_ssm_command(commands):
    result = ssm_client.send_command(
        DocumentName="AWS-RunShellScript",
        Parameters={'commands': commands},
        InstanceIds=[terraform_instance_id],
    )

    return {
        'status': result['Command']['Status'],
        'commandId': result['Command']['CommandId'],
        'statusDetails': result['Command']['StatusDetails']
    }


def build_config(data):
    config_objects = []
    for data_key, data_value in data.items():
        if data_key == 'infra_provider':
            conf_obj = ConfigObj(classifier='provider', conf_type=data_value['provider'], name='')
            conf_obj.add_dependency_variables(dependency={
                'access_key': 'var.app_key["access_key"]',
                'secret_key': 'var.app_key["secret_key"]'
            })
            conf_obj.add_attr(attributes={
                'region': data_value['region']
            })
            config_objects.append(conf_obj)

        if data_key == 'infra_vpc':
            conf_obj = ConfigObj(classifier='resource', conf_type='aws_vpc', name=data_value['name'])
            conf_obj.add_attr(attributes={
                'cidr_block': data_value['cidr_block'],
                'tags': {'Name': data_value['name']}
            })
            config_objects.append(conf_obj)

        if data_key == 'infra_subnet':
            conf_obj = ConfigObj(classifier='resource', conf_type='aws_subnet', name=data_value['name'])
            conf_obj.add_dependency_variables(dependency={
                'vpc_id': f"aws_vpc.{data['infra_vpc']['name']}.id"
            })
            conf_obj.add_attr(attributes={
                'cidr_block': data_value['cidr_block'],
                'tags': {'Name': data_value['name']}
            })
            config_objects.append(conf_obj)

        if data_key == 'infra_server':
            conf_obj = ConfigObj(classifier='resource', conf_type='aws_instance', name=data_value['name'])
            conf_obj.add_dependency_variables(dependency={
                'subnet_id': f"aws_subnet.{data['infra_subnet']['name']}.id"
            })
            conf_obj.add_attr(attributes={
                'ami': ec2_image[data_value['image_id']],
                'instance_type': data_value['instance_type'],
                'tags': {'Name': data_value['name']}
            })
            config_objects.append(conf_obj)

        if data_key == 'infra_database':
            conf_obj = ConfigObj(classifier='resource', conf_type='aws_dynamodb_table', name=data_value['name'])
            conf_obj.add_attr(attributes={
                'name': data_value['table_name'],
                'read_capacity': data_value['read_capacity'],
                'write_capacity': data_value['write_capacity'],
                'hash_key': data_value['partition_key']['name']
            })
            conf_obj.add_config_block(config={
                'attribute': {
                    'name': data_value['partition_key']['name'],
                    'type': data_value['partition_key']['type']
                }
            })
            config_objects.append(conf_obj)

    tf_config = config_objects[0].dump()
    for i in range(1, len(config_objects)):
        tf_config += f'\n\n{config_objects[i].dump()}'
    return tf_config


def build_response(result):
    if result['status'] == 'Success':
        status_code = 200
    elif result['status'] == 'InProgress' or result['status'] == 'Pending':
        status_code = 202
    else:
        status_code = 400

    return {
        'statusCode': status_code,
        'body': json.dumps(
            {
                'commandId': result['commandId'],
                'message': result['statusDetails']
            }
        )
    }
