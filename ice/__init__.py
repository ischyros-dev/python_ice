import boto3

terraform_instance_id = 'i-0f9bebcb45b71d1fe'

ec2_image = {'Amazon Linux 2': 'ami-087c17d1fe0178315',
             'Ubuntu Server 20.04': 'ami-09e67e426f25ce0d7',
             'MS Windows 2022 Server': 'ami-0834c0c835e5946b0'}

ssm_client = boto3.client('ssm')

s3_client = boto3.client('s3')
