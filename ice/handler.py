import json
from ice.utility import (build_config,
                         send_ssm_command,
                         build_response,
                         terraform_instance_id)
from ice import s3_client, ssm_client


def create_resource(event, context):
    tf_config = build_config(json.loads(event['body']))

    commands = [
        f"echo '{tf_config}' > /home/ec2-user/terraspace/infra.tf",
        f"/home/ec2-user/terraform -chdir=/home/ec2-user/terraspace validate",
        f"/home/ec2-user/terraform -chdir=/home/ec2-user/terraspace apply -auto-approve"
    ]

    s3_client.put_object(Body=tf_config, Bucket='dap-test', Key='tf_config')

    result = send_ssm_command(commands)
    return build_response(result)


def destroy_resource(event, context):
    commands = [
        f"/home/ec2-user/terraform -chdir=/home/ec2-user/terraspace validate",
        f"/home/ec2-user/terraform -chdir=/home/ec2-user/terraspace destroy -auto-approve"
    ]

    result = send_ssm_command(commands)

    return build_response(result)


def query_status(event, context):
    data = json.loads(event['body'])

    result = ssm_client.list_command_invocations(CommandId=data['command_id'],
                                                 InstanceId=terraform_instance_id)

    return {
        'statusCode': 200,
        'body': json.dumps(
            {
                'status': result['CommandInvocations'][0]['Status'],
                'message': result['CommandInvocations'][0]['StatusDetails']
            }
        )
    }


def get_config(event, context):
    config_data = ''
    try:
        result = s3_client.get_object(Bucket='dap-test', Key='tf_config')
        config_data = result['Body'].read()
        status = 200
        detail = 'Config fetch successful.'
    except:
        status = 400
        detail = 'Config fetch failed.'

    return {
        'statusCode': status,
        'body': json.dumps(
            {
                'config_data': config_data.decode(),
                'message': detail
            }
        )
    }
